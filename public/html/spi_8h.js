var spi_8h =
[
    [ "SPI_DISABLE", "spi_8h.html#a00174198073dadbaaad7f9475360b228", null ],
    [ "SPI_ENABLE", "spi_8h.html#a27537db92d0837cc77baa0d99b8cac48", null ],
    [ "SPI_LSBFIRST", "spi_8h.html#a7f157edf173ea33cf6ce3995da1720be", null ],
    [ "SPI_MODE0", "spi_8h.html#ac9d0bde2b42b2403d00b7a611514b006", null ],
    [ "SPI_MODE1", "spi_8h.html#a9481b7327b91aa52ff5f70eb2d0acc95", null ],
    [ "SPI_MODE2", "spi_8h.html#a991dec04d2fb38979cfb0d9fc7f8b026", null ],
    [ "SPI_MODE3", "spi_8h.html#a5f928a2445070981765ee0416c12369d", null ],
    [ "SPI_MSBFIRST", "spi_8h.html#ae77748f6ed40c1bc713dbe98e29df4ea", null ],
    [ "spi_init", "spi_8h.html#ae909944aa85ae98323073c628be541aa", null ],
    [ "spi_transfer", "spi_8h.html#afce8db9057c38fd34fb5d18f9030251f", null ]
];