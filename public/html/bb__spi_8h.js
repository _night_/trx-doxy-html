var bb__spi_8h =
[
    [ "BB_MISO_PIN", "bb__spi_8h.html#ae74f28006d9b725541bbce08b0f15591", null ],
    [ "BB_MISO_Px", "bb__spi_8h.html#a4452378a67f4c9a06dc3a288eaa9edee", null ],
    [ "BB_MISO_PxM1", "bb__spi_8h.html#aafd04d2a886bae80f495e3025daba261", null ],
    [ "BB_MISO_PxM2", "bb__spi_8h.html#a971af60207de56c2cb2c73d1572091c6", null ],
    [ "BB_MOSI_PIN", "bb__spi_8h.html#ade4402999e632e4e1dde6c6eae129aee", null ],
    [ "BB_MOSI_Px", "bb__spi_8h.html#a8e42694f35fc626afb85da93ff790147", null ],
    [ "BB_MOSI_PxM1", "bb__spi_8h.html#a78babeafebe2b438a7d7efe2d00b93f8", null ],
    [ "BB_MOSI_PxM2", "bb__spi_8h.html#a9cbfa81f9ab2ef85ac57d143d339d54b", null ],
    [ "BB_SCK_PIN", "bb__spi_8h.html#acfcce186e4b7839f784ceb982c95f511", null ],
    [ "BB_SCK_Px", "bb__spi_8h.html#a8c82f817ed3da992029e82107c65df56", null ],
    [ "BB_SCK_PxM1", "bb__spi_8h.html#a1d3db48b6bf5b48f39fb7775bc3d0e09", null ],
    [ "BB_SCK_PxM2", "bb__spi_8h.html#a791dfa6e033536379b950d8285bb53ba", null ],
    [ "bb_spi_init", "bb__spi_8h.html#a5f474c1af6c0c4412a5c55714dcdf981", null ],
    [ "bb_spi_transfer", "bb__spi_8h.html#add9c161dbfec7e8d4e9f09b53b0bef1d", null ]
];