var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuwy",
  1: "bdfmnpst",
  2: "bdmpst",
  3: "abcdefilmoprstw",
  4: "biu",
  5: "abcdefghilmnpsty"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Macros"
};

